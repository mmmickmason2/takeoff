# frozen_string_literal: true

require './lib/steps/base_roles'
require './lib/steps/unicorn_hup'
require './lib/steps/sidekiq_hup'
require './lib/steps/sidekiq_restart'

module Steps
  class PostDeployMediator < BaseRoles
    # TODO: Refactor this class

    def run
      return unless ran_migrations?

      # Hup unicorn workers to bring in post-deploy migration changes
      Steps::UnicornHup.new(roles, roles_to_hup: roles.service_roles['unicorn']).run

      # send usr1 signal to sidekiq so it stops taking new jobs, then restart it
      Steps::SidekiqHup.new(roles).run
      Steps::SidekiqRestart.new(roles).run
    end

    # TODO: refactor this so it's a mixing
    def ran_migrations?
      roles.run_migrations
    end
  end
end
