# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class Migrations < BaseRoles
    def run
      return unless run_migrations?

      run_command_on_roles roles.blessed_node,
                           migrate_command,
                           title: title
    end

    protected

    def migrate_command
      'sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate'
    end

    def run_migrations?
      roles.run_migrations
    end

    def title
      'Running first batch of migrations from the blessed node'
    end
  end
end
