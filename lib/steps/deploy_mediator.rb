# frozen_string_literal: true

require './lib/steps/base_roles'
require './lib/steps/chef_start'
require './lib/steps/unicorn_hup'
require './lib/steps/services_restart'

module Steps
  class DeployMediator < BaseRoles
    # TODO: Refactor this class

    def run
      roles.regular_roles.each do |role|
        yield_and_wait_until_reload(role, roles.services_for_role(role)) do
          Steps::ChefStart.new(roles, roles_to_start: role).run
          Steps::UnicornHup.new(roles, roles_to_hup: role).run
          Steps::ServicesRestart.new(roles, role: role).run
        end
      end
    end

    private

    def yield_and_wait_until_reload(role, role_services)
      since = Time.now
      services_to_check = role_services.map { |service| "sudo gitlab-ctl status #{service}" }

      yield

      loop do
        sleep(5) unless Takeoff.config[:dry_run]
        cmd = "bundle exec knife ssh -a ipaddress 'roles:#{role}' " \
          "\"#{services_to_check.join(' && ')}\""

        cmd = "echo #{cmd}" if Takeoff.config[:dry_run]

        out = `#{cmd}`
        now = Time.now

        # Bail out once everything has restarted since the given time.
        all_restarted = out.scan(/(\d+)s; run:/).all? do |match|
          (now - match[0].to_i) >= since
        end

        break if all_restarted
      end
    end
  end
end
